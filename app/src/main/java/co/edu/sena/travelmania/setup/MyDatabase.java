package co.edu.sena.travelmania.setup;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase
{
    static final String NAME = "MyDataBase";
    static final int VERSION = 5;
}
