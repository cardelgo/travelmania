package co.edu.sena.travelmania.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.edu.sena.travelmania.R;
import co.edu.sena.travelmania.entity.TouristicEntity;

public class ManageTouristicEntityActivity extends AppCompatActivity
{
    @BindView(R.id.txtManName) TextInputEditText txtManName;
    @BindView(R.id.txtManAddress) TextInputEditText txtManAddress;
    @BindView(R.id.txtManWebSite) TextInputEditText txtManWebSite;
    @BindView(R.id.txtManEmail) TextInputEditText txtManEmail;
    @BindView(R.id.txtManLandLine) TextInputEditText txtManLandLine;
    @BindView(R.id.txtManMobilePhone) TextInputEditText txtManMobilePhone;

    @BindView(R.id.btnDeleteEntity) ImageButton btnDeleteEntity;

    TouristicEntity selectedEntity;

    public static final String ENTITY_KEY = "entity";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_touristic_entity);

        ButterKnife.bind(this);

        selectedEntity = (TouristicEntity) getIntent().getSerializableExtra(ENTITY_KEY);

        if(selectedEntity != null)
        {
            txtManName.setText(selectedEntity.getName());
            txtManAddress.setText(selectedEntity.getAddress());
            txtManWebSite.setText(selectedEntity.getWebSite());
            txtManEmail.setText(selectedEntity.getEmail());
            txtManLandLine.setText(selectedEntity.getLandLine());
            txtManMobilePhone.setText(selectedEntity.getMobilePhone());
        }
        else
        {
            selectedEntity = new TouristicEntity();
            selectedEntity.setType((Integer) getIntent().getSerializableExtra("type"));
            btnDeleteEntity.setVisibility(View.GONE);
        }
    }

    public void btnSaveOnClick(View view)
    {
        selectedEntity.setName(txtManName.getText().toString());
        selectedEntity.setAddress(txtManAddress.getText().toString());
        selectedEntity.setWebSite(txtManWebSite.getText().toString());
        selectedEntity.setEmail(txtManEmail.getText().toString());
        selectedEntity.setLandLine(txtManLandLine.getText().toString());
        selectedEntity.setMobilePhone(txtManMobilePhone.getText().toString());
        selectedEntity.save();

        Toast.makeText(
            view.getContext(), "The entity was saved", Toast.LENGTH_LONG)
            .show();
    }

    public void btnDeleteOnClick(View view)
    {
        new AlertDialog.Builder(this)
            .setMessage("Are you sure you want to delete this record?")
            .setCancelable(false)
            .setPositiveButton("Yes", (dialog, id) -> {
                selectedEntity.delete();
                Toast.makeText(
                    view.getContext(), "The entity was deleted", Toast.LENGTH_LONG)
                    .show();
                ManageTouristicEntityActivity.this.finish();
            })
            .setNegativeButton("No", null)
            .show();
    }

    public void btnWebSite_onClick(View view)
    {
        openBrowser(txtManWebSite.getText().toString());
    }

    private static final String HTTPS = "https://";
    private static final String HTTP = "http://";

    private void openBrowser(String url)
    {
        final Context context = getBaseContext();
        if (!url.startsWith(HTTP) && !url.startsWith(HTTPS)) {
            url = HTTP + url;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(Intent.createChooser(intent, "Choose browser"));// Choose browser is arbitrary :)
    }

    public void btnEmail_onClick(View view)
    {
        String target = txtManEmail.getText().toString();
        String subject = "";
        String message = "\n\nSent from TravelMania";

        sendEmail(target, subject, message);
    }

    private void sendEmail(String target, String subject, String message) {

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{target});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);

        //requiero para hacer prompts email
        email.setType("message/rfc822");

        startActivity(Intent.createChooser(email, "Choose a mail client:"));
    }

    public void btnLandLine_onClick(View view)
    {
        makePhoneCall(txtManLandLine.getText().toString());
    }

    public void btnMobilePhone_onClick(View view)
    {
        makePhoneCall(txtManMobilePhone.getText().toString());
    }

    private void makePhoneCall(String phoneNumber)
    {
        try
        {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return;
            }

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phoneNumber));
            startActivity(callIntent);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void btnWhatsApp_onClick(View view)
    {
        sendWhatsAppMessage(txtManMobilePhone.getText().toString());
    }

    private void sendWhatsAppMessage(String phone)
    {
        final Context context = getBaseContext();
        final String message = "\n\nSent from TravelMania";
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW);

        try
        {
            String url =
                "https://api.whatsapp.com/send?phone=" + phone +"&text="
                    + URLEncoder.encode(message, "UTF-8");
            intent.setPackage("com.whatsapp");
            intent.setData(Uri.parse(url));
            if (intent.resolveActivity(packageManager) != null)
            {
                context.startActivity(intent);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
