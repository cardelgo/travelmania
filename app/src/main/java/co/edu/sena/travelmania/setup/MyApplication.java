package co.edu.sena.travelmania.setup;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowLog;
import com.raizlabs.android.dbflow.config.FlowManager;

import org.json.JSONObject;

import co.edu.sena.travelmania.activity.ManageTouristicEntityActivity;
import co.edu.sena.travelmania.entity.TouristicEntity;
import co.edu.sena.travelmania.model.TouristicEntityModel;

import static co.edu.sena.travelmania.activity.ManageTouristicEntityActivity.ENTITY_KEY;
import static co.edu.sena.travelmania.entity.TouristicEntity.HOTEL;
import static co.edu.sena.travelmania.entity.TouristicEntity.OPERATOR;
import static co.edu.sena.travelmania.entity.TouristicEntity.SITE;

public class MyApplication extends Application
{
    private TouristicEntityModel touristicEntityModel = new TouristicEntityModel();

    @Override
    public void onCreate()
    {
        super.onCreate();
        initDatabase();
        initializeNotifications();
    }

    private void initDatabase()
    {
        // This instantiates DBFlow
        FlowLog.setMinimumLoggingLevel(FlowLog.Level.V);
        // add for verbose logging
        FlowManager.init(new FlowConfig.Builder(this).build());

        populateDatabase();
    }

    private void populateDatabase()
    {
        for (TouristicEntity touristicEntity : touristicEntityModel.getEntities())
        {
            touristicEntity.delete();
        }

        new TouristicEntity(null, "A San Cipriano", SITE, "San Cipriano Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway Railway", "www.sancipriano.com", "info@sancipriano.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "San Antonio", SITE, "Colina arriba", "", "info@sancipriano.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Parque Belalcassar", SITE, "Oriente, en la loma", "", "info@sancipriano.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Bulevar mundialista", SITE, "Sobre el tunel mundialista", "", "info@sancipriano.com", "+57 2 5550000", "+57 2 3166206067").save();

        new TouristicEntity(null, "Hotel Estacion", HOTEL, "Malecón Pacífico # 1-11", "www.hotelestacion.com", "info@hotelestacion.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Spiwak hotel", HOTEL, "Chipichape Avenue # 6-66", "www.hotelspiwak.com", "info@hotelspiwak.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Intercontinental", HOTEL, "Avenida Colombia # 2-20", "www.intercontinental.com", "info@intercontinental.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Hotel Novelty", HOTEL, "Calle 25 # 100-22", "www.hotelnovelty.com", "info@novelty.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Hilton", HOTEL, "Hilton Road # 199", "www.hilton.com", "info@hilton.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Ritss", HOTEL, "Ritss Lane # 777", "www.ritss.com", "info@ritss.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Bell Air", HOTEL, "Bell Air Street# 777", "www.bellair.com", "info@bellair.com", "+57 2 5550000", "+57 2 3166206067").save();

        new TouristicEntity(null, "Belisario Marin", OPERATOR, "Avenida 6 # 56-90", "", "info@belisariomarin.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Decameron", OPERATOR, "Kra 4 # 28-290", "", "info@decameron.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "Tuto Tours", OPERATOR, "Kra 4 # 28-290", "", "info@tutotours.com", "+57 2 5550000", "+57 2 3166206067").save();
        new TouristicEntity(null, "A lo Kike", OPERATOR, "Cordoba City # 12-34", "apple.com", "cardelgo@hotmail.com", "+57 2 3058093", "+57 3163198377").save();
    }

    private void initializeNotifications()
    {
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .setNotificationOpenedHandler(new ManagaEntityNotificationOpenedHandler())
            .init();
    }

    class ManagaEntityNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler
    {
        @Override
        public void notificationOpened(OSNotificationOpenResult result)
        {
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;
            long customKey;

            if (data != null)
            {
                customKey = data.optLong(ENTITY_KEY, -1);
                if (customKey > 0)
                {
                    Log.i("OneSignalExample", "entity set with value: " + customKey);
                    TouristicEntity requestedEntity = touristicEntityModel.getEntityById(customKey);
                    if(requestedEntity != null)
                    {
                        Log.i("OneSignalExample", "Requested entity : " + requestedEntity);
                        Intent intent =
                            new Intent(getApplicationContext(), ManageTouristicEntityActivity.class);
                        intent.setFlags(
                            Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(ENTITY_KEY, requestedEntity);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(
                            MyApplication.this, "Requested entity not found", Toast.LENGTH_SHORT)
                            .show();
                    }
                }
            }

            if (actionType == OSNotificationAction.ActionType.ActionTaken)
            {
                Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);
            }
        }
    }
}
