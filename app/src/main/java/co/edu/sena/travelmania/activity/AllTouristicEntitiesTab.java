package co.edu.sena.travelmania.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.edu.sena.travelmania.R;
import co.edu.sena.travelmania.adapter.TouristicEntityAdapter;
import co.edu.sena.travelmania.entity.TouristicEntity;
import co.edu.sena.travelmania.model.TouristicEntityModel;

import static co.edu.sena.travelmania.activity.ManageTouristicEntityActivity.ENTITY_KEY;
import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

public class AllTouristicEntitiesTab extends AppCompatActivity
{
    @BindView(R.id.lstTouristicEntities) ListView lstTouristicEntities;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.txtSearch) SearchView txtSearchView;
    @BindView(R.id.toolbar) Toolbar toolbar;

    private TouristicEntityModel touristicEntityModel = new TouristicEntityModel();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_touristic_entities_tab);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        loadTabContent(tabLayout.getTabAt(0), "");

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                loadTabContent(tab, txtSearchView.getQuery().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {
            }
        });

        lstTouristicEntities.setOnItemClickListener((parent, view, position, id) -> {
            TextView txtId = view.findViewById(R.id.txtViewId);
            TouristicEntity selectedEntity = (TouristicEntity) txtId.getTag();

            Intent intent = new Intent(getContext(), ManageTouristicEntityActivity.class);
            intent.putExtra(ENTITY_KEY, selectedEntity);

            startActivity(intent);
        });

        txtSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                Log.i("MMM", "query " + query + " en " + tabLayout.getSelectedTabPosition());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                Log.i("MMM", "New " + newText + " en " + tabLayout.getSelectedTabPosition());
                loadTabContent(tabLayout.getTabAt(tabLayout.getSelectedTabPosition()), newText);

                return true;
            }
        });
    }

    private void loadTabContent(TabLayout.Tab tab, String filter)
    {
        Log.i("XXXSSS", tab.getPosition() + "; " + tab.getText() + "; " + tab.getTag());
        TouristicEntityAdapter adapter =
            new TouristicEntityAdapter(
                getContext(),
                touristicEntityModel.getEntitiesOfType(tab.getPosition(), filter));
        lstTouristicEntities.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_all_touristic_entities_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = menuItem.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            Snackbar.make(this.getCurrentFocus(), "Clicked on settings", Snackbar.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    public void btnAddEntityOnClick(View view)
    {
        Intent intent = new Intent(getContext(), ManageTouristicEntityActivity.class);
        intent.putExtra("type", tabLayout.getSelectedTabPosition());

        startActivity(intent);
    }

    @Override
    protected void onResume()
    {
        loadTabContent(
            tabLayout.getTabAt(tabLayout.getSelectedTabPosition()),
            txtSearchView.getQuery().toString());
        super.onResume();
    }
}
