package co.edu.sena.travelmania.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import co.edu.sena.travelmania.R;
import co.edu.sena.travelmania.entity.TouristicEntity;

public class TouristicEntityAdapter extends BaseAdapter
{
    private List<TouristicEntity> listData;
    private LayoutInflater layoutInflater;

    public TouristicEntityAdapter(Context context, List<TouristicEntity> listData)
    {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null)
        {
            convertView = layoutInflater.inflate(R.layout.touristic_entity_view, parent, false);
            holder = new ViewHolder();
            holder.txtViewId = convertView.findViewById(R.id.txtViewId);
            holder.txtViewName = convertView.findViewById(R.id.txtViewName);
            holder.txtViewWebSite = convertView.findViewById(R.id.txtViewWebSite);
            holder.txtViewAddress = convertView.findViewById(R.id.txtViewAddress);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        TouristicEntity selectedEntity = listData.get(position);

        holder.txtViewId.setTag(selectedEntity);
        String labelId = "ID: " + selectedEntity.getId();
        holder.txtViewId.setText(labelId);
        holder.txtViewName.setText(selectedEntity.getName());
        holder.txtViewWebSite.setText(selectedEntity.getWebSite());
        holder.txtViewAddress.setText(selectedEntity.getAddress());

        ImageView btnDeleteEntityFromList = convertView.findViewById(R.id.btnDeleteEntityFromList);
        btnDeleteEntityFromList.setOnClickListener(v ->
            new AlertDialog.Builder(parent.getContext())
                .setMessage("Are you sure you want to delete this record?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> {
                    listData.removeIf(touristicEntity ->
                        touristicEntity.getId().equals(selectedEntity.getId()));
                    selectedEntity.delete();
                    notifyDataSetChanged();
                    Toast.makeText(
                        parent.getContext(), "The entity was deleted", Toast.LENGTH_LONG)
                        .show();
                })
                .setNegativeButton("No", null)
                .show());

        return convertView;
    }

    class ViewHolder
    {
        TextView txtViewId;
        TextView txtViewName;
        TextView txtViewWebSite;
        TextView txtViewAddress;
    }

}