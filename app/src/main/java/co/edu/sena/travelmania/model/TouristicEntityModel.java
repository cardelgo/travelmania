package co.edu.sena.travelmania.model;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;
import java.util.stream.Collectors;

import co.edu.sena.travelmania.entity.TouristicEntity;
import co.edu.sena.travelmania.entity.TouristicEntity_Table;

public class TouristicEntityModel
{
    public List<TouristicEntity> getEntities()
    {
        List<TouristicEntity> entities = SQLite.select().from(TouristicEntity.class).queryList();

        return entities;
    }

    public List<TouristicEntity> getEntitiesOfType(int entityType, String filter)
    {
        List<TouristicEntity> entities =
            SQLite.select().from(TouristicEntity.class)
                .where(TouristicEntity_Table.type.eq(entityType))
                .orderBy(TouristicEntity_Table.name, true)
                .queryList();

        if(filter != null && !"".equals(filter))
        {
            entities =
                entities.stream()
                    .filter(touristicEntity ->
                        touristicEntity.getName().toUpperCase().contains(filter.toUpperCase()))
                    .collect(Collectors.toList());
        }
        return entities;
    }

    public TouristicEntity getEntityById(long entityId)
    {
        TouristicEntity entity =
            SQLite.select().from(TouristicEntity.class)
                .where(TouristicEntity_Table.id.eq(entityId))
                .orderBy(TouristicEntity_Table.name, true)
                .querySingle();

        return entity;
    }
}
