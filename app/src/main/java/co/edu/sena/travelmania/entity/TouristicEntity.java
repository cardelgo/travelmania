package co.edu.sena.travelmania.entity;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

import co.edu.sena.travelmania.setup.MyDatabase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(database = MyDatabase.class)
public class TouristicEntity extends BaseModel implements Serializable
{
    public static final int SITE = 0;
    public static final int HOTEL = 1;
    public static final int OPERATOR = 2;

    @PrimaryKey(autoincrement = true)
    private Long id;

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    private Integer type;

    @Column
    @NotNull
    private String address;

    @Column
    private String webSite;

    @Column
    @NotNull
    private String email;

    @Column
    @NotNull
    private String landLine;

    @Column
    @NotNull
    private String mobilePhone;
}
